# TESSPE

Portail regroupant les ressources pour mes élèves en TES spécialité mathématiques au lycée Mariette pendant la fermeture de l'établissement.


## Organisation :
- Exercices et cours déposés chaque mardi ;
- Correction des exercices le samedi qui suit ;
- Tous les 15 jours, je vous prospose une évaluation via Pronote sur les notions portant sur les exercices corrigés.
- 
En détail :
- Première correction fournie le samedi 21/03 via [Pronote](https://0622949u.index-education.net/pronote/) et [ENT](https://mariette-boulogne-sur-mer.enthdf.fr/).
- Première évaluation le samedi 28/03 via [Pronote](https://0622949u.index-education.net/pronote/).


## Communication:
N’hésitez pas à me poser des questions au besoin (ENT, Pronote, email, ...)
En cas d’indisponibilité de l’[ENT](https://mariette-boulogne-sur-mer.enthdf.fr/), mon mail est celui donné en classe.